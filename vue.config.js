module.exports = {
  css: {
    loaderOptions: {
      sass: {
        data: '@import "@/assets/sass/global-defined.scss";'
      },
    }
  },

  baseUrl: undefined,
  outputDir: undefined,
  assetsDir: undefined,
  runtimeCompiler: undefined,
  productionSourceMap: false,
  parallel: undefined
}
