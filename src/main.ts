// Config Vue
import Vue from 'vue';
import VueAnime from 'vue-animejs';

Vue.use(VueAnime);

// Config Application
import App from './App.vue';

// Import styles
import '@/assets/sass/app.scss';

// Initial Application
Vue.config.productionTip = false;
new Vue({
  render: (h) => h(App),
}).$mount('#app');
