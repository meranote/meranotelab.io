export interface SocialLink {
  name: string;
  alt: string;
  logo: string;
  link: string;
}