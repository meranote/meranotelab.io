import Vue from 'vue';
import { AnimeInstance, AnimeInstanceParams, AnimeParams, AnimeTimelineInstance } from 'animejs';

type AnimeTarget = string | object | HTMLElement | SVGElement | NodeList | null;

/**
 * Because of animejs type definition is using namespace,
 * we need to custom type for compatible to augmented to Vue interface.
 */
declare module "vue/types/vue" {
  interface Vue {
    $anime: {
      // Module constructor
      (params: AnimeParams): AnimeInstance;
      timeline(params?: AnimeInstanceParams | ReadonlyArray<AnimeInstance>): AnimeTimelineInstance;
      // Module helpers
      readonly speed: number;
      readonly running: AnimeInstance[];
      readonly easings: { [EasingFunction: string]: (t: number) => any };
      remove(targets: AnimeTarget | ReadonlyArray<AnimeTarget>): void;
      getValue(targets: AnimeTarget, prop: string): string | number;
      path(path: string | HTMLElement | SVGElement | null, percent?: number): (prop: string) => {
        el: HTMLElement | SVGElement,
        property: string,
        totalLength: number
      };
      setDashoffset(el: HTMLElement | SVGElement | null): number;
      bezier(x1: number, y1: number, x2: number, y2: number): (t: number) => number;
      random(min: number, max: number): number;
    };
  }
}
